import 'package:ferme/bandeau.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class Ferme extends StatefulWidget {
  @override
  _FermeState createState() => _FermeState();
}

class _FermeState extends State<Ferme> {
  @override
  Widget build(BuildContext context) {
    ordi() {
      return Scaffold(
        body: bandeau(),
      );
    }

    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        // Check the sizing information here and return your UI
        if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
          return ordi();
        } else if (sizingInformation.deviceScreenType ==
            DeviceScreenType.tablet) {
          return ordi();
        } else {
          // return phone();
          Navigator.pushNamedAndRemoveUntil(
              context, '/', (Route<dynamic> route) => false);
        }
      },
    );
  }
}
