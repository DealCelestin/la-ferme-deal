import 'package:ferme/bandeau.dart';
import 'package:ferme/main.dart';
import 'package:ferme/ppBar.dart';
import 'package:ferme/ppBar_phone.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:responsive_builder/responsive_builder.dart';

class Histoire extends StatefulWidget {
  @override
  _HistoireState createState() => _HistoireState();
}

bool page = true;
List moment = [
  {
    "date": "2021",
    "text":
        "La ferme Déal est composé de deux associés : Miriam et  Jean-Christophe  et  leurs  trois  salariés  :  Catherine, Anne et Adrien. Ensemble, ils prennent soin  des  20  vaches  laitières  montbéliardes  et  des 97 vaches charolaises, fabriquent autant de fromages qu’il y a de lait et entretiennent 100 hectares de terrain.",
  },
  {
    "date": "2005",
    "text":
        "Miriam, la femme de Jean-Christophe, s’installe. La ferme comptent maintenant 15 vaches laitières et  50  vaches  charolaises.  Cette  année  là,  un  incendit détruit la moitié de la ferme. Miriam & Jean-Christophe vont mettre toutes leurs énergies pour  reconstruire  un  nouveau  batiment  et  une  nouvelle fromagerie.",
  },
  {
    "date": "2004",
    "text": "C’est au tour d’Hélène de partir à la retraite.",
  },
  {
    "date": "1997",
    "text": "Charles part à la retraite.",
  },
  {
    "date": "1987",
    "text":
        "Jean-Christophe,  leur  fils,  les  rejoint.  La  ferme  continue  de  s’agrandir  avec  12  vaches  laitières et 75 hectares de terrain. Ensemble, ils construissent un nouveau batiment pour les vaches charolaises.",
  },
  {
    "date": "1972",
    "text":
        "Hélène & Charles déménage à Saint Romain la Motte, ils ont 4 vaches laitières, 30 vaches charolaises et 35 hectares de terrains.",
  },
  {
    "date": "1965",
    "text":
        "La ferme commence à Saint Edmond. Charles trait quelques vaches laitière à la main et s’occupe des vaches charolaises. Hélène fabrique 30 fromages par jour, qui sont vendus à Charlieu. ",
  },
];

class _HistoireState extends State<Histoire> {
  @override
  Widget build(BuildContext context) {
    // la page histoire est la même pour les ordis et les télephones
    histoire() {
      his(int i) {
        return Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.05,
            ),
            Text(
              moment[i]["date"],
              style: TextStyle(
                  fontFamily: 'Compagnon-Medium',
                  fontSize: 18,
                  color: rouge_ferme),
            ),
            Text(moment[i]["text"],
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline3),
            Container(
              height: 10,
            ),
            Container(
              color: rouge_ferme,
              height: 2,
              width: MediaQuery.of(context).size.width * 0.8,
            ),
          ],
        );
      }

      return Column(
        children: [
          his(0),
          his(1),
          his(2),
          his(3),
          his(4),
          his(5),
          his(6),
        ],
      );
    }

    ordi() {
      aff_photo(String image) {
        return showDialog(
            context: context,
            builder: (BuildContext contexts) {
              return Container(
                  child: GestureDetector(
                onTap: () {
                  Navigator.of(contexts).pop();
                },
                child: PhotoView(
                  imageProvider: AssetImage(image),
                ),
              ));
            });
      }

      galerie() {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    aff_photo('img/histoire/p1.png');
                  },
                  child: Image.asset(
                    'img/histoire/p1.png',
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    aff_photo('img/histoire/p2.png');
                  },
                  child: Image.asset(
                    'img/histoire/p2.png',
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    aff_photo('img/histoire/p3.png');
                  },
                  child: Image.asset(
                    'img/histoire/p3.png',
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    aff_photo('img/histoire/p4.png');
                  },
                  child: Image.asset(
                    'img/histoire/p4.png',
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    aff_photo('img/histoire/p5.png');
                  },
                  child: Image.asset(
                    'img/histoire/p5.png',
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    aff_photo('img/histoire/p6.png');
                  },
                  child: Image.asset(
                    'img/histoire/p6.png',
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () {
                    aff_photo('img/histoire/p7.png');
                  },
                  child: Image.asset(
                    'img/histoire/p7.png',
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    aff_photo('img/histoire/p8.png');
                  },
                  child: Image.asset(
                    'img/histoire/p8.png',
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    aff_photo('img/histoire/p9.jpg');
                  },
                  child: Image.asset(
                    'img/histoire/p9.jpg',
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
              ],
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p10.jpg');
              },
              child: Image.asset(
                'img/histoire/p10.jpg',
                width: MediaQuery.of(context).size.width * 0.25,
              ),
            ),
          ],
        );
      }

      double taille = MediaQuery.of(context).size.width;
      double font = 8 + taille * 0.005;

      body() {
        photo(String photo1, String photo2) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                photo1,
                width: MediaQuery.of(context).size.width * 0.25,
              ),
              Image.asset(
                photo2,
                width: MediaQuery.of(context).size.width * 0.25,
              ),
            ],
          );
        }

        his(int nombre) {
          return Container(
            width: MediaQuery.of(context).size.width * 0.25,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  moment[nombre]["date"],
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontFamily: 'Compagnon-Medium',
                      fontSize: 14,
                      color: rouge_ferme),
                ),
                Text(
                  moment[nombre]["text"],
                  textAlign: TextAlign.start,
                  style: TextStyle(
                      fontFamily: 'Compagnon-Roman',
                      fontSize: taille_info_magazin,
                      color: rouge_ferme),
                ),
              ],
            ),
          );
        }

        return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // bonton
            Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    FlatButton(
                        onPressed: () {},
                        child: Text('Photographies',
                            style: Theme.of(context).textTheme.headline1)),
                    FlatButton(
                        onPressed: () {},
                        child: Text('Chronologie',
                            style: Theme.of(context).textTheme.headline1)),
                  ],
                )),
            Container(
              width: MediaQuery.of(context).size.width * 0.6,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  photo('img/p1.jpg', 'img/p2.jpg'),
                  photo('img/p3.png', 'img/p4.jpg'),
                  photo('img/p5.jpg', 'img/p6.jpg'),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          his(6),
                          his(5),
                          his(4),
                          his(3),
                        ],
                      ),
                      Column(
                        children: [
                          his(2),
                          his(1),
                          his(0),
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        );
      }

      return Scaffold(
          body: SingleChildScrollView(
        child: Column(
          children: [
            bandeau(),
            body(),
          ],
        ),
      ));
    }

    phone() {
      aff_photo(String image) {
        return showDialog(
            context: context,
            builder: (BuildContext context) {
              return Container(
                  child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: PhotoView(
                  imageProvider: AssetImage(image),
                ),
              ));
            });
      }

      galerie() {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p1.png');
              },
              child: Image.asset(
                'img/histoire/p1.png',
              ),
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p2.png');
              },
              child: Image.asset(
                'img/histoire/p2.png',
              ),
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p3.png');
              },
              child: Image.asset(
                'img/histoire/p3.png',
              ),
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p4.png');
              },
              child: Image.asset(
                'img/histoire/p4.png',
              ),
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p5.png');
              },
              child: Image.asset(
                'img/histoire/p5.png',
              ),
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p6.png');
              },
              child: Image.asset(
                'img/histoire/p6.png',
              ),
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p7.png');
              },
              child: Image.asset(
                'img/histoire/p7.png',
              ),
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p8.png');
              },
              child: Image.asset(
                'img/histoire/p8.png',
              ),
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p9.jpg');
              },
              child: Image.asset(
                'img/histoire/p9.jpg',
              ),
            ),
            GestureDetector(
              onTap: () {
                aff_photo('img/histoire/p10.jpg');
              },
              child: Image.asset(
                'img/histoire/p10.jpg',
              ),
            ),
          ],
        );
      }

      return Scaffold(
          appBar: headerphone(context),
          drawer: Drawer(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              FlatButton(
                child: Text(
                  'Histoirique de la ferme Deal',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontFamily: 'Compagnon-Roman', fontSize: 18),
                ),
                onPressed: () {
                  setState(() {
                    page = true;
                    Navigator.of(context).pop();
                  });
                },
              ),
              FlatButton(
                child: Text(
                  'Galerie',
                  style: TextStyle(fontFamily: 'Compagnon-Roman', fontSize: 18),
                ),
                onPressed: () {
                  setState(() {
                    page = false;
                    Navigator.of(context).pop();
                  });
                },
              ),
            ]),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      child: Expanded(child: page ? histoire() : galerie()),
                    ),
                  ],
                ),
                Container(
                  height: 100,
                )
              ],
            ),
          ));
    }

    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        // Check the sizing information here and return your UI
        if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
          return ordi();
        } else if (sizingInformation.deviceScreenType ==
            DeviceScreenType.tablet) {
          return ordi();
        } else {
          return phone();
        }
      },
    );
  }
}
