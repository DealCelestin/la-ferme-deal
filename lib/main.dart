import 'package:ferme/accueil.dart';
import 'package:ferme/ferme.dart';
import 'package:ferme/histoire.dart';
import 'package:ferme/produit.dart';
import 'package:ferme/vendeur.dart';
import 'package:flutter/material.dart';

Color back = _getColorFromHex('F9F6F3');
void main() {
  runApp(new Main());
}

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

Color _getColorFromHex(String hexColor) {
  hexColor = hexColor.replaceAll("#", "");
  if (hexColor.length == 6) {
    hexColor = "FF" + hexColor;
  }
  if (hexColor.length == 8) {
    return Color(int.parse("0x$hexColor"));
  }
}

Color rouge_ferme = _getColorFromHex('C2141F');
List equipe = [
  {
    "nom": "Miriam",
    "poste": "Associé",
    "img": "img/mere.png",
    "text":
        "Après plusieurs années en tant qu’aide-soignante en maison de retraite, j’ai suivi une formation agricole ce qui m’a permis de m’associer à mon mari en 2005. Dans la ferme je suis en charge de la fabrication des fromages pour régaler vos papilles ainsi que la partie gestion et commercialisation. "
  },
  {
    "nom": "Jean-Christophe",
    "poste": "Associé",
    "img": "img/pere.png",
    "text":
        "Passionné par l’agriculture depuis mon plus jeune âge, je décide de m’installer dans la ferme avec mes parents en 1987. Je prends soin de mes deux troupeaux de vaches (laitières et charolaises), je m’occupe également des cultures pour les nourrir ainsi que de la partie administrative. "
  },
  {
    "nom": "Catherine ",
    "poste": "Salariée",
    "img": "img/anne.png",
    "text":
        "Arrivée dans l’aventure en 2015, Catherine s’occupe de la fabrication des fromages, de la préparation des commandes et des livraisons. Nous sommes heureux de la compter dans notre équipe depuis toutes ces années."
  },
  {
    "nom": "Anne",
    "poste": "Salariée",
    "img": "img/anne.png",
    "text":
        "Arrivée en tant qu’apprentie en 2019, Anne a été capable de seconder les deux associés très rapidement dans leurs tâches quotidiennes (ferme et fromagerie un vrai petit couteau-suisse), la garder dans notre équipe était une évidence. "
  },
  {
    "nom": "Adrien",
    "poste": "Salariée",
    "img": "img/adrien.png",
    "text":
        "Dernier arrivé dans l’équipe, il vient une journée par semaine pour apporter son aide à Jean-Christophe (dans les années à venir il est attendu à la fromagerie).   "
  },
];

class _MainState extends State<Main> {
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        textTheme: TextTheme(
          headline1: TextStyle(
              fontFamily: 'Compagnon-Medium', fontSize: 18, color: rouge_ferme),
          headline2: TextStyle(fontFamily: 'Compagnon-Roman', fontSize: 18),
          headline3: TextStyle(
              fontFamily: 'Compagnon-Bold',
              fontSize: 35,
              fontWeight: FontWeight.bold),
        ),
        brightness: Brightness.light,
        primaryColor: rouge_ferme,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => Accueil(),
        '/produit': (context) => Produit(),
        '/histoire': (context) => Histoire(),
        '/vendeur': (context) => Vendeur(),
        '/ferme': (context) => Ferme(),
      },
    );
  }
}
