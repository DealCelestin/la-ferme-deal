import 'package:ferme/accueil.dart';
import 'package:ferme/main.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

bool info_magazin = false;

class bandeau extends StatefulWidget {
  @override
  _bandeauState createState() => _bandeauState();
}

double taille_info_magazin = 10;

class _bandeauState extends State<bandeau> {
  @override
  Widget build(BuildContext context) {
    int bouton = 0;
    double taille_bouton = 10;
    double taille_ecran =
        MediaQuery.of(context).size.height + MediaQuery.of(context).size.width;
    double hauteur_gros_bandeau = 0.104375;
    double logo_hauteur = taille_ecran * 0.03;
    double size_titre = taille_ecran * 0.022;
    double taille_sous_titre = taille_ecran * 0.005;
    return Container(
      color: Colors.black12,
      height: MediaQuery.of(context).size.height * 0.15,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.015625,
            color: Colors.amber,
          ),
          Container(
            child: MouseRegion(
              onEnter: (d) {
                setState(() {
                  info_magazin = true;
                });
              },
              onExit: (d) {
                setState(() {
                  info_magazin = false;
                });
              },
              child: Container(
                height:
                    MediaQuery.of(context).size.height * hauteur_gros_bandeau,
                child: Stack(
                  children: [
                    info_magazin
                        ? Container(
                            child: Row(
                              children: [
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.425,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                      child: Text(
                                        '285 chemin de la roche',
                                        style: TextStyle(
                                            fontFamily: 'Compagnon-Medium',
                                            fontSize: taille_info_magazin,
                                            color: rouge_ferme),
                                      ),
                                      onTap: () async {
                                        if (await canLaunch(
                                            "https://www.google.com/maps/dir/46.0734796,3.9693813/la+ferme+deal/@46.079639,3.9652323,16.33z/data=!4m9!4m8!1m1!4e1!1m5!1m1!1s0x47f40140eaa0ada9:0xf8e5bb7a1429c7a9!2m2!1d3.9638781!2d46.0822603")) {
                                          await launch(
                                              "https://www.google.com/maps/dir/46.0734796,3.9693813/la+ferme+deal/@46.079639,3.9652323,16.33z/data=!4m9!4m8!1m1!4e1!1m5!1m1!1s0x47f40140eaa0ada9:0xf8e5bb7a1429c7a9!2m2!1d3.9638781!2d46.0822603");
                                        }
                                      },
                                    ),
                                    Text(
                                      '42640 Saint Romain la Motte',
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: taille_info_magazin,
                                          color: rouge_ferme),
                                    ),
                                    Text(
                                      '04.77.64.50.96',
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: taille_info_magazin,
                                          color: rouge_ferme),
                                    ),
                                    FlatButton(
                                      child: Text(
                                        'Page Facebook',
                                        style: TextStyle(
                                            fontFamily: 'Compagnon-Medium',
                                            fontSize: taille_info_magazin,
                                            color: rouge_ferme),
                                      ),
                                      onPressed: () async {
                                        if (await canLaunch(
                                            "https://www.facebook.com/La-ferme-D%C3%A9al-357148127955007")) {
                                          await launch(
                                              "https://www.facebook.com/La-ferme-D%C3%A9al-357148127955007");
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        : Container(),
                    info_magazin
                        ? Container(
                            child: Row(
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width *
                                      0.671875,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Le magasin est ouvert',
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: taille_info_magazin,
                                          color: rouge_ferme),
                                    ),
                                    Text(
                                      'tous les jours',
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: taille_info_magazin,
                                          color: rouge_ferme),
                                    ),
                                    Text(
                                      'de 9h00 à 11h30',
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: taille_info_magazin,
                                          color: rouge_ferme),
                                    ),
                                    Text(
                                      'et de 17h00 à 19h00',
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: taille_info_magazin,
                                          color: rouge_ferme),
                                    ),
                                    Text(
                                      'le dimanche et jours fériés',
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: taille_info_magazin,
                                          color: rouge_ferme),
                                    ),
                                    Text(
                                      'de 9h00 à 11h30',
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: taille_info_magazin,
                                          color: rouge_ferme),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          )
                        : Container(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: () {
                            bouton = 0;
                            Navigator.pushNamedAndRemoveUntil(
                                context, '/', (Route<dynamic> route) => false);
                          },
                          child: Image.asset(
                            'img/vacherouge.png',
                            width: logo_hauteur,
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.015625,
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: taille_ecran * 0.0025,
                        ),
                        Container(
                          // width: MediaQuery.of(context).size.width * 0.2,
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  color: back,
                                  child: GestureDetector(
                                    onTap: () {
                                      bouton = 0;
                                      Navigator.pushNamedAndRemoveUntil(context,
                                          '/', (Route<dynamic> route) => false);
                                    },
                                    child: Text(
                                      "LA FERME DÉAL",
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: size_titre,
                                          color: rouge_ferme),
                                    ),
                                  ),
                                ),
                                Transform.translate(
                                  offset: Offset(0, 0),
                                  child: Text(
                                    "Producteurs affineurs de fromages ",
                                    style: TextStyle(
                                        fontFamily: 'Compagnon-Roman',
                                        fontSize: taille_sous_titre,
                                        color: rouge_ferme),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          // Container(
          //   width: MediaQuery.of(context).size.width,
          //   height: 1,
          //   color: rouge_ferme,
          // ),
          Container(
            height: (MediaQuery.of(context).size.height * 0.03) - 1,
            child: Row(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.34375,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.1640625,
                  child: FlatButton(
                      padding: EdgeInsets.all(0),
                      onPressed: () {
                        bouton = 1;
                        Navigator.pushNamedAndRemoveUntil(
                            context, '/ferme', (Route<dynamic> route) => false);
                      },
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: bouton == 1
                            ? Text('notre ferme',
                                style: TextStyle(
                                    fontFamily: 'Compagnon-Bold',
                                    fontSize: taille_bouton,
                                    color: rouge_ferme,
                                    fontWeight: FontWeight.bold))
                            : Text('NOTRE FERME',
                                style: Theme.of(context).textTheme.headline1),
                      )),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.1640625,
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      bouton = 2;
                      Navigator.pushNamedAndRemoveUntil(context, '/histoire',
                          (Route<dynamic> route) => false);
                    },
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: bouton == 2
                          ? Text('notre histoire',
                              style: TextStyle(
                                  fontFamily: 'Compagnon-Bold',
                                  fontSize: taille_bouton,
                                  color: rouge_ferme,
                                  fontWeight: FontWeight.bold))
                          : Text('NOTRE HISTOIRE',
                              style: Theme.of(context).textTheme.headline1),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.1640625,
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      bouton = 3;
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/produit', (Route<dynamic> route) => false);
                    },
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: bouton == 3
                          ? Text('nos produits',
                              style: TextStyle(
                                  fontFamily: 'Compagnon-Bold',
                                  fontSize: taille_bouton,
                                  color: rouge_ferme,
                                  fontWeight: FontWeight.bold))
                          : Text('NOS PRODUITS',
                              textAlign: TextAlign.start,
                              style: Theme.of(context).textTheme.headline1),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.1640625,
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      bouton = 4;
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/vendeur', (Route<dynamic> route) => false);
                    },
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: bouton == 4
                          ? Text('ou nous trouver?',
                              style: TextStyle(
                                  fontFamily: 'Compagnon-Bold',
                                  fontSize: taille_bouton,
                                  color: rouge_ferme,
                                  fontWeight: FontWeight.bold))
                          : Text('OU NOUS TROUVER?',
                              textAlign: TextAlign.start,
                              style: Theme.of(context).textTheme.headline1),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
