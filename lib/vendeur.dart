import 'package:ferme/bandeau.dart';
import 'package:ferme/main.dart';
import 'package:ferme/ppBar.dart';
import 'package:ferme/ppBar_phone.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

final List client = [
  {
    "lieu": "Route de Vivans, 42310 La Pacaudière",
    "site":
        "https://magasins.supermarches-atac.fr/magasin/la-pacaudiere/70142?utm_source=GMB&utm_campaign=Multidiffusion&utm_content=70142",
    "nom": "ATAC",
    "phone": "04 77 64 10 20"
  },
  {
    "lieu": " Rue Jean de la Fontaine, 42300 Mably",
    "site": "https://www.carrefour.fr/magasin/mably",
    "nom": "Carrefour Mably",
    "phone": "04 77 44 17 79"
  },
  {
    "lieu": "2 Rue du Tacot, 42370 Renaison",
    "site": "https://www.carrefour.fr/magasin/market-renaison",
    "nom": "Carrefour Market Renaison",
    "phone": "04 77 66 49 14"
  },
  {
    "lieu": "53 Rue Jules Faron, 42153 Riorges",
    "site": "https://www.carrefour.fr/magasin/market-riorges/drive",
    "nom": "Carrefour Market riorges",
    "phone": "04 77 71 16 03"
  },
  {
    "lieu": "Rue Alexandre Raffin, 42300 Roanne",
    "site": "https://www.facebook.com/espacesaintlouisroanne/",
    "nom": "Carrefour Market Saint Louis",
    "phone": "04 77 67 87 05"
  },
  {
    "lieu": "N 7 Trouver mon itinéraire, 42120 Le Coteau",
    "site": "https://www.carrefour.fr/magasin/market-le-coteau",
    "nom": "Carrefour Market Le Coteau",
    "phone": "04 77 68 61 55"
  },
  {
    "lieu": "69 Avenue Jean Moos, 69550 Amplepuis",
    "site": "https://www.carrefour.fr/magasin/market-amplepuis",
    "nom": "Carrefour Market amplepuis",
    "phone": "04 74 89 48 00"
  },
  {
    "lieu": "44 Rue Roger Salengro, 42300 Roanne",
    "site": "",
    "nom": "Crèmerie des Halles",
    "phone": "04 77 68 08 52"
  },
  {
    "lieu": " 64 Rue de Trebande, 42640 Saint-Romain-la-Motte",
    "site": "https://magasins.vival.fr/fr/s/vival-saint-romain-la-motte",
    "nom": "Epicerie vival",
    "phone": "04 77 64 56 53"
  },
  {
    "lieu": "Route de Roanne, 69550 Amplepuis",
    "site": "https://www.intermarche.com/?utm_source=gmb",
    "nom": "Intermarché  Amplepuis",
    "phone": "04 74 89 25 28"
  },
  {
    "lieu": "Rue du Commerce, 42120 Perreux",
    "site": "",
    "nom": "Le Renouveau",
    "phone": "04 77 68 52 60"
  },
  {
    "lieu": "131 Rue de Verdun, 42640 Saint-Germain-Lespinasse",
    "site": "",
    "nom": "Les Frangines",
    "phone": "04 77 64 50 22"
  },
  {
    "lieu": "21 pl Verdun, 42300 MABLY",
    "site": "",
    "nom": "Le Verdun",
    "phone": "04 77 71 52 93"
  },
  {
    "lieu": "27 Rue Etienne Thinon, 42370 Saint-Alban-les-Eaux",
    "site": "http://lesgensdici.asso.fr/",
    "nom": "Les gens d'ici",
    "phone": "04 77 65 81 81"
  },
  {
    "lieu": "31 Rue des Manufacturiers, 42640 Saint-Romain-la-Motte",
    "site": "",
    "nom": "Le temps des saisons",
    "phone": "04 77 60 92 38"
  },
  {
    "lieu": "1 Rue St Alban, 42153 Riorges",
    "site": "",
    "nom": "Maison chapon",
    "phone": "04 77 71 42 99"
  },
  {
    "lieu": "Rue de Gruyères, 42370 Renaison",
    "site": "https://www.boucherie-gonin.fr/",
    "nom": "Maison Gonin",
    "phone": "04 77 62 11 08"
  },
  {
    "lieu": "21 Route de Briennon, 42300 Mably",
    "site": "http://www.boucheriedessables.fr/",
    "nom": "Maison Mollon",
    "phone": "04 77 69 58 52"
  },
  {
    "lieu": "4 Boulevard Camille Benoît, 42300 Roanne",
    "site": "https://magasin.netto.fr/166-netto-roanne?utm_source=gmb",
    "nom": "Netto roanne",
    "phone": "04 77 23 00 95"
  },
  {
    "lieu": "RN 7, 42300 Mably",
    "site": "https://magasin.netto.fr/165-netto-mably?utm_source=gmb",
    "nom": "Netto Mably",
    "phone": "04 77 23 19 12"
  },
  {
    "lieu": "2715 Route de Roanne, 42640 Saint-Romain-la-Motte",
    "site": "https://www.tlmenparle.fr/",
    "nom": "Tout le monde en parle",
    "phone": "04 77 52 22 22"
  },
];

class Vendeur extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    body() {
      double taille_ecran = MediaQuery.of(context).size.height +
          MediaQuery.of(context).size.width;

      return Row(
        children: [
          Container(
            padding:
                EdgeInsets.all(MediaQuery.of(context).size.width * 0.015625),
            width: MediaQuery.of(context).size.width * 0.34375,
            child: Expanded(
                child: Image.asset(
              'img/map.png',
              fit: BoxFit.fitHeight,
            )),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                color: Colors.deepPurpleAccent,
                width: MediaQuery.of(context).size.width * 0.1484375,
                child: Column(
                  children: [
                    Text(
                      'Saint Romain La Motte\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'Tout le monde en parle\n2715 Route de Roanne,\n42640 Saint-Romain la motte\n04 77 52 22 22\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    Text(
                      'Epicerie vival\n64 Rue de Trebande,\n42640 Saint-Romain la motte\n04 77 64 56 53\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    Text(
                      'Le temps des saisons\n31 Rue des Manufacturiers,\n42640 Saint Romain la motte\n04 77 60 92 38',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    // Saint germain Lespinasse
                    Text(
                      'Saint Germain Lespinasse\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'Les Frangines\n131 Rue de Verdun,\n42640 Saint-Germain lespinasse\n04 77 64 50 22\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    // Renaison
                    Text(
                      'Renaison\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'Carrefour Market Renaison\n2 Rue du Tacot,\n42370 Renaison\n04 77 66 49 14\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.015625,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.1484375,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Maison Gonin\nRue de Gruyères,\n42370 Renaison\n04 77 62 11 08\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    // Saint Alban Les Eaux
                    Text(
                      'Saint Alban Les Eaux\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      '27 Rue Etienne Thinon,\n42370 Saint-Alban-les-Eaux\n04 77 65 81 81\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    // Mably
                    Text(
                      'Mably\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'Carrefour Mably\nRue Jean de la Fontaine,\n 42300 Mably\n04 77 44 17 79',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    Text(
                      'Netto Mably\nRN 7,\n42300 Mably\n04 77 23 19 12\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    // Perreux
                    Text(
                      'Perreux\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'Le Renouveau\nRue du Commerce,\n42120 Perreux\n,04 77 68 52 60\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    // Riorges
                    Text(
                      'Riorges\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'Maison chapon\n1 Rue St Alban,\n42153 Riorges\n04 77 71 42 99\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.015625,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.1484375,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Carrefour Market riorges\n53 Rue Jules Faron,\n42153 Riorges\n04 77 71 16 03\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    // Roanne
                    Text(
                      'Roanne\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'Crèmerie des Halles\n44 Rue Roger Salengro,\n42300 Roanne\n04 77 68 08 52',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    Text(
                      'Carrefour Market\nSaint Louis\nRue Alexandre Raffin,\n42300 Roanne\n04 77 67 87 05',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    Text(
                      'Netto roanne\n4 Boulevard Camille Benoît,\n 42300 Roanne\n04 77 23 00 95\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    // Le Coteau
                    Text(
                      'Le Coteau\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'Carrefour Market Le Coteau\nN 7 Trouver mon itinéraire,\n42120 Le Coteau\n04 77 68 61 55\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.015625,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.1484375,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // La Pacaudière
                    Text(
                      'La Pacaudière\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'ATAC\nRoute de Vivans,\n42310 La Pacaudière\n04 77 64 10 20\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    // Amplepuis
                    Text(
                      'Amplepuis\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Roman',
                          color: rouge_ferme,
                          fontSize: 14),
                    ),
                    Text(
                      'Carrefour Market amplepuis\n9 Avenue Jean Moos,\n69550 Amplepuis\n04 74 89 48 00\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                    Text(
                      'Intermarché  Amplepuis\nRoute de Roanne,\n69550 Amplepuis\n04 74 89 25 28\n',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          fontFamily: 'Compagnon-Medium',
                          fontSize: 14,
                          color: rouge_ferme),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.015625,
              ),
            ],
          )
        ],
      );
    }

    ordi() {
      return Scaffold(
          body: Column(
        children: [
          bandeau(),
          body(),
        ],
      ));
    }

    phone() {
      return Scaffold(
          appBar: headerphone(context),
          body: Row(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 02.15,
              ),
              Container(
                child: Expanded(
                  child: ListView.builder(
                      itemCount: client.length,
                      itemBuilder: (BuildContext context, int i) {
                        String url = client[i]["site"];
                        return Column(
                          children: [
                            Container(
                              height: 10,
                            ),
                            Center(
                              child: Container(
                                // color: rouge_ferme,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      client[i]["nom"],
                                      style: TextStyle(
                                          fontFamily: 'Compagnon-Medium',
                                          fontSize: 18,
                                          color: rouge_ferme),
                                    ),
                                    Text(
                                      client[i]["lieu"],
                                      textAlign: TextAlign.center,
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                    FlatButton(
                                      onPressed: () {
                                        UrlLauncher.launch(
                                            'tel:${client[i]["phone"]}');
                                      },
                                      child: Text(client[i]["phone"],
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4),
                                    ),
                                    url.isNotEmpty
                                        ? Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              FlatButton(
                                                  onPressed: () async {
                                                    print(client[i]["site"]);
                                                    if (await canLaunch(url)) {
                                                      await launch(url);
                                                    }
                                                  },
                                                  child: Text("lien du site",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .headline4)),
                                            ],
                                          )
                                        : Container()
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 10,
                            ),
                            Container(
                              color: rouge_ferme,
                              height: 2,
                              width: MediaQuery.of(context).size.width,
                            ),
                          ],
                        );
                      }),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.15,
              ),
            ],
          ));
    }

    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        // Check the sizing information here and return your UI
        if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
          return ordi();
        } else if (sizingInformation.deviceScreenType ==
            DeviceScreenType.tablet) {
          return ordi();
        } else {
          return phone();
        }
      },
    );
  }
}
